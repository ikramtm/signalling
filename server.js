
var app = require('express')();
const express = require ('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);

const PORT = process.env.PORT || 3000;

app.use(express.static('public'));

var numUsers = 0;

// var io = socketIO.listen(http);
io.sockets.on('connection', function(socket) {

  // convenience function to log server messages on the client
  function log() {
    var array = ['Message from server:'];
    array.push.apply(array, arguments);
    socket.emit('log', array);
  }

  socket.on('message', function(message) {
    log('Client said: ', message);
    // for a real app, would be room-only (not broadcast)
    socket.broadcast.emit('message', message);
  });

  socket.on('create or join', function(room) {
    log('Received request to create or join room ' + room);
    socket.join(room);
    var num = io.sockets.adapter.rooms[room];
    numUsers = num.length;
    log('Room ' + room + ' now has ' + numUsers + ' client(s)');

    if (numUsers === 1) {
      socket.join(room); 
      log('Client ID ' + socket.id + ' created room ' + room);
      socket.emit('created', room, socket.id);

    } else if (numUsers === 2) {
      log('Client ID ' + socket.id + ' joined room ' + room);
      io.sockets.in(room).emit('join', room);
      socket.join(room);
      socket.emit('joined', room, socket.id);
      io.sockets.in(room).emit('ready');
    } else { // max two clients
      socket.emit('full', room);
    }

    socket.in(room).on('test',(data)=>{
      console.log(data)
      socket.to(room).emit('meh',data);
    });

     //RTC exchange protocol
    socket.in(room).on('sdp change',(data)=>{
      console.log('offering sdp');
      socket.to(room).emit('offer',{type:"offer",offer:data.offer});
    });

    socket.in(room).on('answer',(answer)=>{
      console.log('remote sdp answer');
      socket.to(room).emit('remote response',{answer:answer.answer})
    });

    socket.in(room).on('ice handling',(candidate)=>{
      console.log('emit local ice');
      socket.to(room).emit('candidate',{type:'icecandidate', candidate:candidate})
    })

    socket.in(room).on('remote ice response',(candidate)=>{
      console.log('remote ice exchange');
      socket.to(room).emit('remote candidate', {type:'icecandidate', candidate:candidate})
    })

  });

  
  socket.on('disconnect', function () {
      // --numUsers;
      socket.broadcast.emit('user left');
  });

  socket.on('bye', function(){
    // numUsers--;
    console.log('received bye');
  });

  socket.on('ipaddr', function() {
    var ifaces = os.networkInterfaces();
    for (var dev in ifaces) {
      ifaces[dev].forEach(function(details) {
        if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
          socket.emit('ipaddr', details.address);
        }
      });
    }
  });
});

http.listen(PORT, function(){
  console.log('listening on *:3000');
});