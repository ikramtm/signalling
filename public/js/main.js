
        //variable declaration
        let localConnection;
        let pcConstraint;
        let receiveChannel;
        let sendChannel;
        let dataConstraint;
        let room = '';
        let localIsCaller = false;
        let roomFull = false;
        let localStream

        const yourVideo = document.querySelector('#yours');
        const theirVideo = document.querySelector('#theirs')
        const start = document.querySelector('#start');
        const callBtn = document.querySelector('#call');
        const hangupBtn = document.querySelector('#hang-up');
        const dataChannelSend = document.querySelector('#dataChannelSend')

        let yourConnection;
        let theirConnection;
        
        callBtn.disabled = true;
        hangupBtn.disabled = true;

        //create a room
        start.onclick = function(e) {
            room = prompt("Please enter the room you want to join.")
            e.preventDefault();
            if (room.length !== 0){
                //initiate signalling
                initConnection(room);
            }
            else{
                alert('Please enter the room number or room is full')
            }
        }

        callBtn.addEventListener('click',()=>{
            localIsCaller = true;
            startPeerConnection(localStream);
        })

        let socket = io.connect();

        function initConnection() {
            signalling(room);
            //disable start button    
            start.disabled = true;
            callBtn.disabled = false;
            hangupBtn.disabled = false;
        }

        //signalling logic
        function signalling(room){

            if (room !== '') {
                socket.emit('create or join', room);
                console.log('Attempted to create or join room', room);
            }

            socket.on('created', function(room) {
                console.log('Created room ' + room);
                isInitiator = true;
                connecting();
            });

            socket.on('full', function(room) {
                console.log('Room ' + room + ' is full');
                roomFull = true;
                alert('Sorry, the room is full')
            });

            socket.on('join', function (room){
                console.log('Another peer made a request to join room ' + room);
                console.log('This peer is the initiator of room ' + room + '!');
                isChannelReady = true;
            });

            socket.on('joined', function(room) {
                console.log('joined: ' + room);
                isChannelReady = true;
                connecting();
            });

            socket.on('log', function(array) {
                console.log.apply(console, array);
            });

            socket.on('disconnect', function () {
                console.log('you have been disconnected');
            });
        }

        window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection ||
            window.webkitRTCPeerConnection || window.msRTCPeerConnection;
        window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription ||
            window.webkitRTCSessionDescription || window.msRTCSessionDescription;
        navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia ||
            navigator.webkitGetUserMedia || navigator.msGetUserMedia;
        window.SignallingServer = window.SignallingServer;

        function connecting(){
            if (hasUserMedia()) {
                  navigator.getUserMedia({ video: true, audio: true }, function (myStream) {
                    console.log('accessing camera');
                    localStream = myStream;

                    yourVideo.src = window.URL.createObjectURL(localStream);
                    if (hasRTCPeerConnection()) {
                        // setupPeerConnection(stream);
                    } else {
                        alert("Sorry, your browser does not support WebRTC.");
                    }
                }, function (error) {
                    console.log(error);
                });
              } else {
                    alert("Sorry, your browser does not support WebRTC.");
                }
                
        }

        function hasUserMedia() {
              navigator.getUserMedia = navigator.getUserMedia ||
              navigator.webkitGetUserMedia || navigator.mozGetUserMedia ||
              navigator.msGetUserMedia;
              return !!navigator.getUserMedia;
        }

        function hasRTCPeerConnection() {
              window.RTCPeerConnection = window.RTCPeerConnection ||
              window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
              window.RTCSessionDescription = window.RTCSessionDescription ||
              window.webkitRTCSessionDescription ||
              window.mozRTCSessionDescription;
              window.RTCIceCandidate = window.RTCIceCandidate ||
              window.webkitRTCIceCandidate || window.mozRTCIceCandidate;
              return !!window.RTCPeerConnection;
        }

        function setupPeerConnection(stream) {
                // let configuration = {
                //     "iceServers": [{ 
                //             "url": "stun:stun.1.google.com:19302" 
                //         },
                //         {
                //             "url":"turn:numb.viagenie.ca:3478",
                //             "credential":"ballprk",
                //             "username":"ikram@ballprk.com"
                //         }]
                // };
                // yourConnection = new RTCPeerConnection(configuration);
                // // Setup stream listening
                // yourConnection.addStream(stream);
                // yourConnection.onaddstream = function (e) {
                //     theirVideo.src = window.URL.createObjectURL(e.stream);
                // };

                // Setup ice handling
                // yourConnection.onicecandidate = function (event) {
                //     if (event.candidate) {
                //         socket.emit('ice handling',{
                //             type: "candidate",
                //             candidate: event.candidate
                //         });
                //     } 
                // };
                
        }
        
        function startPeerConnection(stream){
            socket.emit('test','FREEDOOOOOOOM')
            let configuration = {
                    "iceServers": [{ 
                            "url": "stun:stun.1.google.com:19302" 
                        },
                        {
                            "url":"turn:numb.viagenie.ca:3478",
                            "credential":"ballprk",
                            "username":"ikram@ballprk.com"
                        }]
                };
            yourConnection = new RTCPeerConnection(configuration);
            // Setup stream listening
            yourConnection.addStream(stream);
            // yourConnection.onaddstream = function (e) {
            //     theirVideo.src = window.URL.createObjectURL(e.stream);
            // };
            yourConnection.createOffer((offer)=>{
                socket.emit('sdp change',{
                    type:"offer",
                    offer:offer
                });
                yourConnection.setLocalDescription(offer);
            },function(error){
                alert("An error has occurred.");
            });   
        }

        socket.on('offer',(data)=>{
            onOffer(data);
        })
        function onOffer(data){
            let configuration = {
                    "iceServers": [{ 
                            "url": "stun:stun.1.google.com:19302" 
                        },
                        {
                            "url":"turn:numb.viagenie.ca:3478",
                            "credential":"ballprk",
                            "username":"ikram@ballprk.com"
                        }]
                };
            if(data){
                theirConnection = new RTCPeerConnection(configuration)
                if (hasUserMedia()) {
                  navigator.getUserMedia({ video: true, audio: true }, function (myStream) {
                    console.log('accessing camera');
                    stream = myStream;
                    yourVideo.src = window.URL.createObjectURL(stream);
                    if (hasRTCPeerConnection()) {
                        theirConnection.addStream(stream);
                        theirConnection.onaddstream = function (e) {
                            theirVideo.src = window.URL.createObjectURL(e.stream);
                        };
                    } else {
                        alert("Sorry, your browser does not support WebRTC.");
                    }
                }, function (error) {
                    console.log(error);
                });
                }
                theirConnection.setRemoteDescription(new RTCSessionDescription(data.offer));
                theirConnection.createAnswer(function (answer){
                    // yourConnection.setLocalDescription(answer);
                    // console.log(answer)
                    theirConnection.setLocalDescription(answer);
                    socket.emit('answer',{
                        type:"answer",
                        answer:answer
                    });
                },function(error){
                    alert("An error has occurred.");
                });
            }
        }

        //remote answer call
        socket.on('remote response',(data)=>{
            console.log('got it ', data.answer);
            onAnswer(data);
        });

        function onAnswer(data){
            console.log('processing remote SDP for caller')
            yourConnection.setRemoteDescription(new RTCSessionDescription(data.answer));
            yourConnection.onicecandidate = function (event) {
                    if (event.candidate) {
                        console.log('sending local ice to remote')
                        socket.emit('ice handling',{
                            type: "candidate",
                            candidate: event.candidate
                        });
                    } 
                };
        }

        socket.on('candidate',(candidate)=>{
            onCandidate(candidate.candidate.candidate)
        })

        function onCandidate(candidate){
            console.log('processing caller SDP')
            theirConnection.addIceCandidate(new RTCIceCandidate(candidate));
            theirConnection.onicecandidate = function (event) {
                    if (event.candidate) {
                        console.log('sending remote ice response');
                        socket.emit('remote ice response',{
                            type: "candidate",
                            candidate: event.candidate
                        });
                    }                    
                };
        };

    
        socket.on('remote candidate',(candidate)=>{
            if (localIsCaller){
                console.log('remote ice accepted');
                localonCandidate(candidate.candidate.candidate)
            }
        });

        function localonCandidate(candidate){
            console.log('accepting remote ice');
            yourConnection.addIceCandidate(new RTCIceCandidate(candidate));
            yourConnection.onaddstream = function (e) {
                theirVideo.src = window.URL.createObjectURL(e.stream);
            };
        };

        